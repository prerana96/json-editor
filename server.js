var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
const http = require('http');
var xmlParser = require('xml-js');
var cors = require('cors');
const mariadb = require('mariadb');
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8000;
console.log('server listening at port:', port)        // set our port

// ROUTES FOR OUR API
var router = express.Router();
http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('okay');
});
// http.createServer(options, app).listen(PORT, () => logger('debug', `your server is running on ${PORT}`)); 


app.use(cors());
app.use(router);

router.post('/fetchschema', function (req, res) {

var obj=req.body;

const toJsonSchema = require('to-json-schema');
 

const schema = toJsonSchema(obj);

res.send(schema);
  
})

//WIP
router.post('/saveData', function (req, res) {

  const db = mariadb.createPool({
    host: 'localhost', 
    user:'root', 
    password: 'root',
    database:'test',
    connectionLimit: 5
  });
  db.getConnection()
      .then(conn => {
        conn.query("select * from t1")
        .then((rows) => {
          console.log(rows); //[ {val: 1}, meta: ... ]
          //Table must have been created before 
          // " CREATE TABLE myTable (id int, val varchar(255)) "
          //return conn.query("INSERT INTO myTable value (?, ?)", [1, "mariadb"]);
        })
      }).catch(err => {
        //not connected
       console.log('not connected');
      });
})

app.listen(port);

