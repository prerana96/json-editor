//import React from 'react';
import './App.css';
import React from 'react';

class Form extends React.Component {
  render () {
    return (
     
      <form onSubmit = {this.retrieve}>
        
      <h2>Enter a JSON Object in textbox below</h2>
      <textarea id="inputText" placeholder="Enter here"></textarea>
      <button type="submit" id="btnSubmit">Submit</button> 
   </form>
    )
   
  }

  retrieve = async(event) => {
    event.preventDefault();
    var xhr = new XMLHttpRequest()
    //get a callback when the server responds
    xhr.addEventListener('load', () => {
      // update the state of the component with the result here
      // console.log(this.profileData)
      this.resp = xhr.responseText;
      //var val = Id.render(this.profileData);
      //return (val);
      this.generateHtml(this.resp);
    })
    this.val = document.getElementById('inputText').value;
    console.log(this.val);
    // open the request with the verb and the url
    xhr.open('POST', 'http://localhost:8000/fetchschema')
    // send the request
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(this.val);
    
  }

  generateHtml(schemaVal) {
 
    document.getElementById('inputText').remove();
    document.getElementById('btnSubmit').remove();
    document.getElementById('editor_holder').innerHTML="";
   var editor = new  window.JSONEditor(document.getElementById('editor_holder'), {
      theme: 'bootstrap4',
      
     //  disable_edit_json: true,
      // disable_properties: true,
      schema: JSON.parse(schemaVal),
      "startval":JSON.parse(this.val)
    });
    var btn = document.createElement("button");   // Create a <button> element
    btn.innerHTML = "Save to DB";     
    btn.onclick = () => { 
      var xhr = new XMLHttpRequest;
      xhr.addEventListener('load', () => {
        console.log(xhr.responseText);
      })
      xhr.open('POST', 'http://localhost:8000/saveData')
      //xhr.setRequestHeader();
      xhr.send()
      
    }              // Insert text
    document.body.appendChild(btn);
    console.log(editor)
  }
}

class App extends React.Component {
  constructor(props) {
   // Id = new Id();
    super(props);
    this.profileData = "tj";
  }

  render() {
    
    return (
      <div className="App">
        <Form />
        <div id="editor_holder"></div>
        {/* <Card /> */}
      </div>
    );
  }
}


export default App;
